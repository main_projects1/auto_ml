# AutoMl pipeline for binary classification

Проект для задачи бинарной классификации по подготовке датасета, выбору наилучших гиперпараметров и обучения на загруженном датасете. Возможен выбор одной из двух метрик для оценки качества обученной модели.


## Структура проекта

* **/src** - скрипты для automl классификации:
  * **train.py** - скрипт запуска выбора наилучшей модели и обучения по датасету;
  * **predict.py** - скрипт предсказывания значений по натренированной модели.
  
    
* **req.txt** - файл .txt c зависимостями.


## Установка

1. Склонировать репозиторий проекта:

`$ git clone git@gitlab.com:main_projects1/auto_ml.git`

2. Установить необходимые Python-библиотеки:

`$ pip install -r req.txt`


## Запуск
### Обработка датасета и обучение 
Обучение происходит на модели, которая дает наилучший результат по выбранной метрике.
1. Сохранить датасет в формате .csv

2. Для запуска auto_ml pipeline необходимо перейти в директорию /src/ и выполнить команду:

`$ python train.py --train-csv /path/to/dataset --scoring f1 --model-path /path/to/save/model --metrics-models [True, False]`

Описание параметров запуска доступно по команде:

`$ python train.py -h`
```console
usage: train.py [-h] --train-csv /path/to/dataset [--scoring scoring_function] [--model-path /path/to/save/model] [--metrics-models 'False' or 'True']

Choose the best hyperparameter for autoML pipeline and train dataset for binary classification.

optional arguments:
  -h, --help            show this help message and exit
  --train-csv /path/to/dataset
                        Path to .csv file of dataset
  --scoring scoring_function
                        'balanced_accuracy' or 'f1'
  --model-path /path/to/save/model
                        Path to .pkl file to save model
  --metrics-models 'False' or 'True'
                        Get the list with results of another models on metric

```
В результате работы скрипта по указанному пути --model-path сохранится pipeline модели, которая имеет наиболее высокий показатель по выбранной метрике. 

При указании параметра --metrics-models в качестве True, в директории с моделью появится .csv файл с результатами других моделей, обученных на загруженном датасете.

### Предсказывание 
2. Для получения предсказаний по обученной модели необходимо перейти в директорию /src/ и выполнить команду:

`$ python predict.py --predict-csv /path/to/predict/dataset --model-path /path/to/trained/model --output-path /path/to/save/predict`

Описание параметров запуска доступно по команде:

`$ python predict.py -h`
```console
usage: predict.py [-h] --predict-csv /path/to/predict/dataset/ [--model-path path/to/trained/model] [--output-path path/to/save/predict]

Predict value on trained model.

optional arguments:
  -h, --help            show this help message and exit
  --predict-csv /path/to/predict/dataset/
                        Path to .csv file of dataset for prediction
  --model-path path/to/trained/model
                        Path to .pkl file where model was saved
  --output-path path/to/save/predict
                        Path to .csv file to save predictions

```

В результате работы скрипта по указанному параметру -output-path будет создан .csv файл с результатами предсказания.