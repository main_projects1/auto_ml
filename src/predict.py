"""

This script is used to do prediction based on trained model.

------------------------------------------------------------

Usage: run from the command line:

    #Predict values for dataset
    python predict.py --predict-csv /path/to/predict/dataset --model-path /path/to/trained/model --output-path /path/to/save/predict

"""

import joblib
import argparse
import os
from pathlib import Path
import pandas as pd

############################################################
#  Predict values
############################################################


if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Predict value on trained model.')
    parser.add_argument('--predict-csv', type=argparse.FileType('r'), required=True,
                        metavar='/path/to/predict/dataset/',
                        help="Path to .csv file of dataset for prediction")
    parser.add_argument('--model-path', default='../best_model/model.pkl',
                        metavar='path/to/trained/model',
                        help='Path to .pkl file where model was saved')
    parser.add_argument('--output-path', default='../predict/prediction.csv',
                        metavar='path/to/save/predict',
                        help='Path to .csv file to save predictions')
    args = parser.parse_args()

    # Validate arguments
    if not os.path.exists(Path(args.model_path)):
        exit("Provide --model-path to use model")

    # Load model
    loaded_model = joblib.load(Path(args.model_path))

    # Load dataset and make predictions
    data_pred = pd.read_csv(args.predict_csv)
    prediction = loaded_model.predict(data_pred)

    # Save results
    output_path = Path(args.output_path)
    if os.path.exists(output_path):
        os.remove(output_path)

    output_path.parent.mkdir(parents=True, exist_ok=True)
    pd.DataFrame(prediction).to_csv(output_path)  # concat table predict with predictions

    print(f"\n Persisted predictions to {output_path}")
