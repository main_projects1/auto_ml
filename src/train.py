"""

AutoML pipeline to fit the best model for binary classification on custom dataset.

------------------------------------------------------------

Usage: run from the command line:

    #Build autoML pipeline on dataset
    python train.py --train-csv /path/to/dataset --scoring f1 --model-path /path/to/save/model --metrics-models [True, False]

"""

import argparse
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.compose import ColumnTransformer, make_column_selector
from sklearn.preprocessing import StandardScaler, MinMaxScaler, RobustScaler, OneHotEncoder
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.model_selection import RandomizedSearchCV
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC, SVC
import os
from pickle import dump
from pathlib import Path
import shutil


class MyAutoMLClassifier:
    def __init__(self, scoring_function='f1', n_iter=50):
        self.scoring_function = scoring_function
        self.n_iter = n_iter

    def fit(self, X, y):
        X_train = X
        y_train = y

        categorical_values = []

        cat_subset = X_train.select_dtypes(include=['object', 'category', 'bool'])

        for i in range(cat_subset.shape[1]):
            categorical_values.append(list(cat_subset.iloc[:, i].dropna().unique()))

        num_pipeline = Pipeline([
            ('cleaner', SimpleImputer()),
            ('scaler', StandardScaler())
        ])

        cat_pipeline = Pipeline([
            ('cleaner', SimpleImputer(strategy='most_frequent')),
            ('encoder', OneHotEncoder(sparse=False, categories=categorical_values))
        ])

        preprocessor = ColumnTransformer([
            ('numerical', num_pipeline, make_column_selector(dtype_exclude=['object', 'category', 'bool'])),
            ('categorical', cat_pipeline, make_column_selector(dtype_include=['object', 'category', 'bool']))
        ])

        model_pipeline_steps = []
        model_pipeline_steps.append(('preprocessor', preprocessor))
        model_pipeline_steps.append(('feature_selector', SelectKBest(f_classif, k='all')))
        model_pipeline_steps.append(('estimator', LogisticRegression()))
        model_pipeline = Pipeline(model_pipeline_steps)

        total_features = preprocessor.fit_transform(X_train).shape[1]

        optimization_grid = []

        # Logistic regression
        optimization_grid.append({
            'preprocessor__numerical__scaler': [RobustScaler(), StandardScaler(), MinMaxScaler()],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [LogisticRegression()]
        })

        # K-nearest neighbors
        optimization_grid.append({
            'preprocessor__numerical__scaler': [RobustScaler(), StandardScaler(), MinMaxScaler()],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [KNeighborsClassifier()],
            'estimator__weights': ['uniform', 'distance'],
            'estimator__n_neighbors': np.arange(1, 20, 1)
        })

        # Random Forest
        optimization_grid.append({
            'preprocessor__numerical__scaler': [None],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [RandomForestClassifier(random_state=0)],
            'estimator__n_estimators': np.arange(5, 500, 10),
            'estimator__criterion': ['gini', 'entropy']
        })

        # Gradient boosting
        optimization_grid.append({
            'preprocessor__numerical__scaler': [None],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [GradientBoostingClassifier(random_state=0)],
            'estimator__n_estimators': np.arange(5, 500, 10),
            'estimator__learning_rate': np.linspace(0.1, 0.9, 20),
        })

        # Decision tree
        optimization_grid.append({
            'preprocessor__numerical__scaler': [None],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [DecisionTreeClassifier(random_state=0)],
            'estimator__criterion': ['gini', 'entropy']
        })

        # Linear SVM
        optimization_grid.append({
            'preprocessor__numerical__scaler': [RobustScaler(), StandardScaler(), MinMaxScaler()],
            'preprocessor__numerical__cleaner__strategy': ['mean', 'median'],
            'feature_selector__k': list(np.arange(1, total_features, 5)) + ['all'],
            'estimator': [LinearSVC(random_state=0)],
            'estimator__C': np.arange(0.1, 1, 0.1),

        })

        search = RandomizedSearchCV(
            model_pipeline,
            optimization_grid,
            n_iter=self.n_iter,
            scoring=self.scoring_function,
            n_jobs=-1,
            random_state=0,
            # verbose=3,
            cv=5
        )

        search.fit(X_train, y_train)
        self.best_estimator_ = search.best_estimator_
        self.best_pipeline = search.best_params_

        self.random_results = pd.concat([pd.DataFrame(search.cv_results_["params"]),
                                       pd.DataFrame(search.cv_results_["mean_test_score"], columns=[self.scoring_function])], axis=1).sort_values(self.scoring_function, ascending=False)


############################################################
#  AutoML Pipeline with training model
############################################################


if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Choose the best hyperparameter for autoML pipeline and train dataset for binary classification.')
    parser.add_argument('--train-csv', type=argparse.FileType('r'), required=True,
                        metavar="/path/to/dataset",
                        help="Path to .csv file of dataset")
    parser.add_argument('--scoring', choices=['balanced_accuracy', 'f1'], default='f1',
                        metavar="scoring_function",
                        help="'balanced_accuracy' or 'f1'")
    parser.add_argument('--model-path', default='../best_model/model.pkl',
                        metavar="/path/to/save/model",
                        help="Path to .pkl file to save model")
    parser.add_argument('--metrics-models', type = bool, default=False,
                        metavar="'False' or 'True'",
                        help="Get the list with results of another models on metric")

    args = parser.parse_args()

    # Load dataset
    df = pd.read_csv(args.train_csv)
    print('Dataset read, shape {}'.format(df.shape))

    # Prepare dataset
    X = df.drop('target', axis=1)
    y = df['target']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    # Launch autoML pipeline and fit model
    model_automl = MyAutoMLClassifier(args.scoring)
    model_automl.fit(X_train, y_train)

    print('\n', 'Best model pipeline for the dataset:', '\n')
    [print(feature, ':', var) for feature, var in model_automl.best_pipeline.items()]

    # Persist model
    model_path = Path(args.model_path)

    if os.path.exists(model_path.parent):
        shutil.rmtree(model_path.parent)

    model_path.parent.mkdir(parents=True, exist_ok=True)

    with open(model_path, "wb") as f:
        dump(model_automl.best_estimator_, f)

    print(f"\n Persisted model to {model_path}")

    # Get the list of results another models
    if args.metrics_models:
        model_automl.random_results.to_csv(f'{str(model_path.parent)}/models_results.csv')

        print(f"\n Persisted list with results of models to {model_path.parent}/models_results.csv")
